#!/usr/bin/bash
macaddress="/sys/class/net/enp2s0/address"
januscfg="/usr/local/etc/janus/janus.cfg"
read -r mac < "$macaddress"
sed -i "s|;server_name\s=\sMyJanusInstance|server_name=${mac}|g" "$januscfg"
