## janus libraries

apt-get -y install libmicrohttpd-dev libjansson-dev libnice-dev \
	libssl-dev libsrtp-dev libsofia-sip-ua-dev libglib2.0-dev \
	libopus-dev libogg-dev libcurl4-openssl-dev liblua5.3-dev \
	pkg-config gengetopt libtool automake
# install librstp
wget https://github.com/cisco/libsrtp/archive/v2.2.0.tar.gz
tar xfv v2.2.0.tar.gz
cd libsrtp-2.2.0
./configure --prefix=/usr --enable-openssl
make shared_library && sudo make install
# install libwebsockets
git clone git://git.libwebsockets.org/libwebsockets
cd libwebsockets
mkdir build
cd build
# See https://github.com/meetecho/janus-gateway/issues/732 re: LWS_MAX_SMP
cmake -DLWS_MAX_SMP=1 -DCMAKE_INSTALL_PREFIX:PATH=/usr -DCMAKE_C_FLAGS="-fpic" ..
make && sudo make install
# install janus
git clone https://tbui@bitbucket.org/tbui/janus-gateway.git
cd janus-gateway
sh autogen.sh
./configure
make && make install
make configs
# set the server name in the config file
./set_servername_to_macaddress.sh